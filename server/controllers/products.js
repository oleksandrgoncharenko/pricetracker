import express from 'express';
import mongoose from 'mongoose';
import cheerio from 'cheerio';
import axios from'axios';

import ProductItem from '../models/ProductItem.js';
import {pricesFromStore} from '../store/stores.js';

const router = express.Router();

export const getProducts = async (req, res) => { 
    try {
        const ProductItems = await ProductItem.find({creator: req.userId});
        ProductItems.forEach(async(item)=>{
            const html = await axios.get(item.link);
            const $ =  await cheerio.load(html.data);
            const price =  pricesFromStore[item.store]($);
            if(Number(item.current_price) > Number(price)){
                item.previous_price = item.current_price;
                item.current_price = price;
                item.message = 'DISCOUNT';
            }
            else {
                item.current_price = price;
            }
            await ProductItem.findByIdAndUpdate(item.id, item, { new: true });
        });

        const ProductItemsWithUpdates = await ProductItem.find({creator: req.userId});

        res.status(200).json(ProductItemsWithUpdates);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

export const getProduct = async (req, res) => { 
    const { id } = req.params;

    try {
        const product = await ProductItem.findById(id);
        
        res.status(200).json(product);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

export const createProduct = async (req, res) => {
    const product = req.body;
    const newProductItem = new ProductItem({ ...product, creator: req.userId, createdAt: new Date().toISOString() })
    try {
        const html = await axios.get(newProductItem.link);
        const $ =  await cheerio.load(html.data);
        const price =  pricesFromStore[newProductItem.store]($);
        newProductItem.current_price = price;
        await newProductItem.save();
        res.status(201).json(newProductItem );
    } catch (error) {
        res.status(409).json({ message: error.message });
    }
}

export const updateProduct = async (req, res) => {
    const { id } = req.params;
    const { name, store,country, link, creator } = req.body;
    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No product with id: ${id}`);

    const updatedProduct = { name, store, link,country, _id: id, creator };
    const html = await axios.get(updatedProduct.link);
    const $ =  await cheerio.load(html.data);
    const price =  pricesFromStore[updatedProduct.store]($);
    updatedProduct.current_price = price;
    await ProductItem.findByIdAndUpdate(id, updatedProduct, { new: true });

    res.json(updatedProduct);
}

export const deleteProduct = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No product with id: ${id}`);

    await ProductItem.findByIdAndRemove(id);

    res.json({ message: "Product deleted successfully." });
}


export default router;