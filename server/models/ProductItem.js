import mongoose from 'mongoose';

const productSchema = mongoose.Schema({
    name: String,
    message: {
        type: String,
        default: '',
    },
    link: String,
    country: String,
    store: String,
    current_price: {
        type: String,
        default: '0',
    },
    previous_price: {
        type: String,
        default: '0',
    },
    creator: String,
    reatedAt: {
        type: Date,
        default: new Date(),
    },
})

var ProductItem = mongoose.model('ProductItem', productSchema);

export default ProductItem;