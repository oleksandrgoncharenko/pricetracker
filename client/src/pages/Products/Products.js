import React, { useState, useEffect } from 'react'
import ProductForm from "./ProductForm";
import PageHeader from "../../components/PageHeader";
import PeopleOutlineTwoToneIcon from '@material-ui/icons/PeopleOutlineTwoTone';
import { Paper, makeStyles, TableBody, TableRow, TableCell, Toolbar, InputAdornment } from '@material-ui/core';
import useTable from "../../components/useTable";
import Controls from "../../components/controls/Controls";
import { Search } from "@material-ui/icons";
import AddIcon from '@material-ui/icons/Add';
import RefreshIcon from '@material-ui/icons/Refresh';
import Popup from "../../components/Popup";
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import CloseIcon from '@material-ui/icons/Close';
import Notification from "../../components/Notification";
import ConfirmDialog from "../../components/ConfirmDialog";
import {getProducts, createProduct, updateProduct, deleteProduct} from "../../actions/products";
import {useDispatch, useSelector} from "react-redux";

const useStyles = makeStyles(theme => ({
    pageContent: {
        margin: theme.spacing(0),
        padding: theme.spacing(3)
    },
    searchInput: {
        width: '65%'
    },
    newButton: {
        position: 'absolute',
        right: '10px'
    },
    refreshButton: {
        position: 'absolute',
        right: 200
    }
}))


const headCells = [
    { id: 'name', label: 'Product Name' },
    { id: 'link', label: 'Product link' },
    { id: 'country', label: 'Country' },
    { id: 'store', label: 'Store' },
    { id: 'current_price', label: 'Current Price' },
    { id: 'previous_price', label: 'Previous Price' },
    { id: 'message', label: 'Message' },
]

export default function Products() {
  const dispatch = useDispatch();

    const classes = useStyles();
    const [recordForEdit, setRecordForEdit] = useState(null)
    const products = useSelector((state) => state.products);
    const [records, setRecords] = useState(products);
    useEffect(() => {
        dispatch(getProducts());
      }, [records]);
    const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })
    const [openPopup, setOpenPopup] = useState(false)
    const [notify, setNotify] = useState({ isOpen: false, message: '', type: '' })
    const [confirmDialog, setConfirmDialog] = useState({ isOpen: false, title: '', subTitle: '' })
    

     


    const {
        TblContainer,
        TblHead,
        TblPagination,
        recordsAfterPagingAndSorting
    } = useTable(products, headCells, filterFn);

    const handleSearch = e => {
        let target = e.target;
        setFilterFn({
            fn: items => { 
                if (target.value == "")
                    return items;
                else
                    return items.filter(x => x.name.toLowerCase().includes(target.value))
            }
        })
    }
    const addOrEdit = (product, resetForm) => {
        if (product.id == 0)
            dispatch(createProduct(product));
        if (product._id !== 0 && product.id !=0)
            dispatch(updateProduct(product));
        resetForm()
        setRecordForEdit(null)
        setOpenPopup(false)
        setRecords(dispatch(getProducts()))
        setNotify({
            isOpen: true,
            message: 'Submitted Successfully',
            type: 'success'
        })
    }

    const openInPopup = item => {
        setRecordForEdit(item)
        setOpenPopup(true)
    }

    const onDelete = id => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteProduct(id));
        setRecords(dispatch(getProducts()))
        setNotify({
            isOpen: true,
            message: 'Deleted Successfully',
            type: 'error'
        })
    }

    return (
        <>
            <PageHeader
                title="New Product"
                subTitle="Form design with validation"
                icon={<PeopleOutlineTwoToneIcon fontSize="large" />}
            />
            <Paper className={classes.pageContent}>

                <Toolbar>
                    <Controls.Input
                        label="Search Products"
                        className={classes.searchInput}
                        InputProps={{
                            startAdornment: (<InputAdornment position="start">
                                <Search />
                            </InputAdornment>)
                        }}
                        onChange={handleSearch}
                    />
                    <Controls.Button
                        text="Add New"
                        variant="outlined"
                        startIcon={<AddIcon />}
                        className={classes.newButton}
                        onClick={() => { setOpenPopup(true); setRecordForEdit(null); }}
                    />
                    <Controls.Button
                        text="Refresh"
                        variant="outlined"
                        startIcon={<RefreshIcon />}
                        className={classes.refreshButton}
                        onClick={() => { setRecords(dispatch(getProducts())); }}
                    />
                </Toolbar>
                <TblContainer>
                    <TblHead />
                    <TableBody>
                        {recordsAfterPagingAndSorting().map(item =>
                                (<TableRow key={item._id}>
                                    <TableCell>{item.name}</TableCell>
                                    <TableCell>{item.link}</TableCell>
                                    <TableCell>{item.country}</TableCell>
                                    <TableCell>{item.store}</TableCell>
                                    <TableCell>{item.current_price}</TableCell>
                                    <TableCell>{item.previous_price}</TableCell>
                                    <TableCell>{item.message}</TableCell>
                                    <TableCell>
                                        <Controls.ActionButton
                                            color="primary"
                                            onClick={() => { openInPopup(item) }}>
                                            <EditOutlinedIcon fontSize="small" />
                                        </Controls.ActionButton>
                                        <Controls.ActionButton
                                            color="secondary"
                                            onClick={() => {
                                                setConfirmDialog({
                                                    isOpen: true,
                                                    title: 'Are you sure to delete this record?',
                                                    subTitle: "You can't undo this operation",
                                                    onConfirm: () => onDelete(item._id)
                                                })
                                            }}>
                                            <CloseIcon fontSize="small" />
                                        </Controls.ActionButton>
                                    </TableCell>
                                </TableRow>)
                            )
                        }
                    </TableBody>
                </TblContainer>
                <TblPagination />
            </Paper>
            <Popup
                title="Product Form"
                openPopup={openPopup}
                setOpenPopup={setOpenPopup}
            >
                <ProductForm
                    recordForEdit={recordForEdit}
                    addOrEdit={addOrEdit} />
            </Popup>
            <Notification
                notify={notify}
                setNotify={setNotify}
            />
            <ConfirmDialog
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog}
            />
        </>
    )
}

