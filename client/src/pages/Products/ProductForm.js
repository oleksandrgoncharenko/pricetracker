import React, { useState, useEffect } from 'react'
import { Grid, } from '@material-ui/core';
import Controls from "../../components/controls/Controls";
import { useForm, Form } from '../../components/useForm';



const storeItems = [
    { id: 'ua_epicenter', title: 'epicenter' },
    { id: 'pl_allegro', title: 'allegro' },
]

const countryItems = [
    { id: 'ua', title: 'ukraine' },
    { id: 'pl', title: 'poland' },

]

const initialFValues = {
    id: 0,
    name: '',
    link: '',
    country: '',
    store: '',
}

const getStores = (country)=>{
    return storeItems.filter(x=> {return x['id'].toString().includes(country||''.toLowerCase() + '_')})
    }

export default function ProductForm(props) {
    const { addOrEdit, recordForEdit } = props


    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('name' in fieldValues)
            temp.name = fieldValues.name ? "" : "This field is required."
        if ('link' in fieldValues)
            temp.link = fieldValues.link ? "" : "This field is required."
        if ('store' in fieldValues)
            temp.store = fieldValues.store ? "" : "This field is required."
        if ('country' in fieldValues)
            temp.country = fieldValues.country ? "" : "This field is required."      
        setErrors({
            ...temp
        })

        if (fieldValues == values)
            return Object.values(temp).every(x => x == "")
    }

    const {
        values,
        setValues,
        errors,
        setErrors,
        handleInputChange,
        resetForm
    } = useForm(initialFValues, true, validate);

    const handleSubmit = e => {
        e.preventDefault()
        if (validate()) {
            addOrEdit(values, resetForm);
        }
    }

    useEffect(() => {
        if (recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    }, [recordForEdit])

    return (
        <Form onSubmit={handleSubmit}>
            <Grid container>
                <Grid item xs={6}>
                    <Controls.Input
                        name="name"
                        label="Name"
                        value={values.name}
                        onChange={handleInputChange}
                        error={errors.name}
                    />
                    <Controls.Input
                        label="Link"
                        name="link"
                        value={values.link}
                        onChange={handleInputChange}
                        error={errors.link}
                    />

                </Grid>
                <Grid item xs={6}>
                <Controls.Select
                        name="country"
                        label="Country"
                        value={values.country}
                        onChange={handleInputChange}
                        options={countryItems}
                        error={errors.country}
                    />
                    <Controls.Select
                        name="store"
                        label="Store"
                        value={values.store}
                        onChange={handleInputChange}
                        options={getStores(values.country)}
                        error={errors.store}
                    />

                    <div>
                        <Controls.Button
                            type="submit"
                            text="Submit" />
                        <Controls.Button
                            text="Reset"
                            color="default"
                            onClick={resetForm} />
                    </div>
                </Grid>
            </Grid>
        </Form>
    )
}
