import React, { useEffect, useState }  from 'react';
import './App.css';
import SideMenu from "../components/SideMenu";
import { makeStyles, CssBaseline, createMuiTheme, ThemeProvider } from '@material-ui/core';
import Header from "../components/Header";
import PageHeader from '../components/PageHeader';

import Products from "../pages/Products/Products";
import Navbar from '../pages/Navbar/Navbar';
import Auth from '../pages/Auth/Auth';

import { Store } from '@material-ui/icons';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { useSelector} from "react-redux";



const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#333996",
      light: '#3c44b126'
    },
    secondary: {
      main: "#f83245",
      light: '#f8324526'
    },
    background: {
      default: "#f4f5fd"
    },
  },
  overrides:{
    MuiAppBar:{
      root:{
        transform:'translateZ(0)'
      }
    }
  },
  props:{
    MuiIconButton:{
      disableRipple:true
    }
  }
})


const useStyles = makeStyles({
  appMain: {
    paddingLeft: '100px',
    width: '100%'
  }
})

const showProducts = (isValid)=>{
if(isValid==true) {
  return Products;
}
}

function App() {
  const classes = useStyles();
  const isLoggedIn = useSelector(() => JSON.parse(localStorage.getItem('profile')) !== null);

  return (
    <BrowserRouter>
    <ThemeProvider theme={theme}>
      <div className={classes.appMain}>
      <Navbar />
      <Switch>
        <Route path="/auth" exact component={Auth} />
        <Route path="/" exact component={showProducts(isLoggedIn)}/>
        }
      </Switch>
      </div>
      <CssBaseline />
    </ThemeProvider>
    </BrowserRouter>
  );
}


export default App;
